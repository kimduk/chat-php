/**
 * Created by kimduk on 15.04.14.
 */

var conn, text, template;

function connect(server, port) {
    conn = new WebSocket('ws://' + server + ":" + port);
    template = Handlebars.compile($("#message").html());

    conn.onmessage = function (e) {
        var messages = JSON.parse(e.data);

        for(var i in messages) {
            $(".container").append(template.messages[1]);

            scroll();
        }
    }
}

function scroll() {
    var doc = $(document);
    doc.scrollTop(doc.height());
}

$(function () {
    text = $('#text');
    $('form').submit(function (event) {
        // Когда нажат Enter, отправляем сообщение.
        conn.send(text.val());

        // Очищаем поле ввода.
        text.val('');

        // Отменяем отправку формы.
        return false;
    });
    scroll();
});