<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 15.04.14
 * Time: 9:02
 */

require __DIR__ . "/vendor/autoload.php";

use Silex\Provider\SessionServiceProvider;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\MemcacheSessionHandler;

$app = new Elfet\Chat\Application();

$app->get("/", function () use ($app) {
    return $app->render("chat.phtml", [
        "user" => $app["user"]
    ]);
})->bind("index");



$app["facebook"] = $app->share(function() use ($app) {
    return new \Facebook([
        "appId" => "",
        "secret" => "",
        "allowSignedRequest" => false
    ]);
});

$app->register(new SessionServiceProvider());

$app["session.storage.handler"] = $app->share(function ($app) {
    $memcache = new \Memcache();
    $memcache->connect("localhost", 11211);
    return new MemcacheSessionHandler($memcache);
});

$app['user'] = function () use ($app) {
    return $app['session']->get('user');
};

$app->before(function($request) use ($app) {
    $user = $app["user"];
    if (null === $user) {
        $facebook = $app["facebook"];
        $result = $facebook->api(array(
            "method" => "fql.query",
            "query" => "SELECT uid, name, pic_square, profile_url FROM user WHERE uid = me()"
        ));

        if (!empty($result)) {
            $app["session"]->set("user", $result[0]);
            return;
        }

        return $app->render("login.phtml", [
            "loginUrl" => $facebook->getLoginUrl()
        ]);
    }
});

$app->run();