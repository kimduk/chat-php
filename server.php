<?php

use Elfet\Chat\Server;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\Session\SessionProvider;
use Ratchet\WebSocket\WsServer;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\MemcacheSessionHandler;

require __DIR__ . '/vendor/autoload.php';
/*
$memcache = new Memcache();
$memcache->connect("localhost", 11211);

$sessionHandler = new MemcacheSessionHandler($memcache);

//$chat = new Server();
$sessionProvider = new SessionProvider($server, $sessionHandler);
$websoket = new WsServer($sessionProvider);

$http = new HttpServer($websoket);
$server = IoServer::factory($http, 8080, '127.0.0.1');

$server->run();
*/

$memcache = new Memcache;
$memcache->connect('localhost', 11211);

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new SessionProvider(
                new Server(),
                new MemcacheSessionHandler($memcache)
            )
        )
    ),
    8080, '127.0.0.1'
);

$server->run();