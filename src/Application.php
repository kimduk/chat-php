<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 15.04.14
 * Time: 9:49
 */

namespace Elfet\Chat;

use Silex\Application\UrlGeneratorTrait;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\MemcacheSessionHandler;
use Whoops\Provider\Silex\WhoopsServiceProvider;

class Application extends \Silex\Application
{
    public function render($viewPath, $params = [])
    {
        extract($params);

        ob_start();
        include $viewPath;
        $content = ob_get_clean();

        return new Response($content);
    }
}